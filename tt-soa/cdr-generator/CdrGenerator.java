import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This utility generates a CDR file from the given input data. Could be used in
 * various projects
 * 
 * @author Douraid Arfaoui
 *
 */
public class CdrGenerator {

	private static final SimpleDateFormat filenameFormatter = new SimpleDateFormat("yyyMMdd");
	private static final SimpleDateFormat timestampFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final Date now = new Date();

	/**
	 * Usage: CdrGenerator.generate("/cdr/directory/path",arg1,arg2,...,argn).
	 * 
	 * @param cdrDirPath
	 *            The target directory where the CDR will be generated
	 * @param data
	 *            CDR line content represented by different arguments.
	 * @throws CdrGenerationFailedException
	 */
	public static void generate(String cdrDirPath, Object... data) throws CdrGenerationFailedException {
		now.setTime(System.currentTimeMillis());
		String cdrFilePath = cdrDirPath + filenameFormatter.format(now);
		File cdrFile = new File(cdrFilePath);
	

		try {
			if (!cdrFile.exists())
				cdrFile.createNewFile();
			/* Adding timestamp at the beginning of the line */
			String line = timestampFormatter.format(now);

			for (Object param : data) {
				line = line.concat(" | ".concat(String.valueOf(param)));
			}
			FileWriter fw = new FileWriter(cdrFilePath, true);
		    BufferedWriter bw = new BufferedWriter(fw);
		    PrintWriter out = new PrintWriter(bw);
		    out.println(line);
		    out.close();
		    bw.close();
		    fw.close();

		} catch (IOException exp) {
			throw new CdrGenerationFailedException(exp);
		}

	}

}
