/**
 * Exception type that should be thrown whenever a CDR (Call Data Record) generation is failed
 * @author Douraid Arfaoui
 *
 */
public class CdrGenerationFailedException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public CdrGenerationFailedException(String message){
		super(message);
	}
	
	public CdrGenerationFailedException(String message, Throwable e){
		super(message, e);
	}
	
	public CdrGenerationFailedException(Throwable e){
		super(e);
	}

}
